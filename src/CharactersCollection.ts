import { Sorter } from "./Sorter/Sorter";

export class ChractersCollection extends Sorter {
  constructor(public data: string) {
    super();
  }

  get length(): number {
    return this.data.length;
  }

  compare(leftIndex: number, rightIndex: number): boolean {
    return (
      this.data[leftIndex].toLowerCase() > this.data[rightIndex].toLowerCase()
    );
  }

  swap(leftIndex: number, rightIndex: number): void {
    const characters = this.data.split("");

    const booble = characters[leftIndex];
    characters[leftIndex] = characters[rightIndex];
    characters[rightIndex] = booble;

    this.data = characters.join("");
  }
}
