import { LinkedList } from "./LinkedList";
import { Sorter } from "../Sorter/Sorter";

export class LinkedListCollection extends Sorter {
  constructor(public data: LinkedList) {
    super();
  }

  get length() {
    return this.data.length;
  }

  compare = (leftIndex: number, rightIndex: number) =>
    this.data.at(leftIndex).data > this.data.at(rightIndex).data;

  swap = (leftIndex: number, rightIndex: number) => {
    const leftNode = this.data.at(leftIndex);
    const rightNode = this.data.at(rightIndex);

    const leftHand = leftNode.data;
    leftNode.data = rightNode.data;
    rightNode.data = leftHand;
  }
}
