import { ChractersCollection } from "../CharactersCollection";
import { LinkedList } from "../LInkedList/LinkedList";
import { LinkedListCollection } from "../LInkedList/LinkedListCollection";
import { NumbersCollection } from "../NumbersCollection";

describe("sortable tests", () => {
  test("sort number array", () => {
    const numberCollection = new NumbersCollection([10, 3, -5, 0]);

    numberCollection.sort();

    expect([-5, 0, 3, 10]).toStrictEqual(numberCollection.data);
  });

  test("sort string", () => {
    const charactersCollection = new ChractersCollection("Xaaybc");

    charactersCollection.sort();

    expect(charactersCollection.data).toBe("aabcXy");
  });

  test("sort linked list", () => {
    const given = new LinkedList();
    given.add(10);
    given.add(3);
    given.add(-5);
    given.add(0);
    const linkedListCollection = new LinkedListCollection(given);

    linkedListCollection.sort();

    expect(linkedListCollection.data.printable()).toBe("-5 -> 0 -> 3 -> 10");
  });
});
